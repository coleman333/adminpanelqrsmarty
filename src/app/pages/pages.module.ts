import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {routing} from './pages.routing';
import {NgaModule} from '../theme/nga.module';
import {AppTranslationModule} from '../app.translation.module';

import {Pages} from './pages.component';
import {AuthGuard} from './guards/auth.guard'
import {AdminGuard} from './guards/admin.guard';
import {NoAuthGuard} from './guards/noauth.guard';
import { UserDataResolver } from './guards/UserDataResolver';
import { UsersService } from './users';
import { HttpClient } from './httpClient';

@NgModule({
  imports: [CommonModule, AppTranslationModule, NgaModule, routing],
  declarations: [Pages],
  providers: [
    AuthGuard, AdminGuard, NoAuthGuard, UserDataResolver, UsersService, HttpClient
  ]
})
export class PagesModule {
}
