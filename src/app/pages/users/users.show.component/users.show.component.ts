import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { SmartTables } from '../../general.components/smartTables/smartTables.component';
import { SETTINGS } from '../table.settings';
import { UsersService } from '../users.service';
import { Users } from '../Users';

@Component({
  moduleId: module.id,
  selector: 'users-component',
  templateUrl: 'users.show.component.html',
  styleUrls: ['users.show.component.scss'],
})
export class UsersComponent implements OnInit, OnDestroy {

  @ViewChild(SmartTables)
  tables: SmartTables;

  private usersObservable: any;
  private editObservable: any;
  private deleteObservable: any;

  constructor(private service: UsersService, private router: Router) {
    this.editItem = this.editItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  ngOnInit() {
    this.usersObservable = this.service.getUsers()
      .map(users => users.json().map(user => new Users(user)))
      .subscribe(users => {
        // console.log(users);
        this.tables.setSourse(users, SETTINGS);
      });
  }

  editItem(data) {
    this.editObservable = this.service.updateUsers(data, data.id)
      .subscribe( res => {});
  }

  deleteItem(usersId) {
    this.deleteObservable = this.service.deleteUsers(usersId)
      .subscribe( res => {});
  }

  selectItem(usersId) {
    this.router.navigate(['/pages/users/users/', usersId ]);
  }

  ngOnDestroy() {
    if (this.usersObservable) {
      this.usersObservable.unsubscribe();
    }
    if (this.editObservable) {
      this.editObservable.unsubscribe();
    }
    if (this.deleteObservable) {
      this.deleteObservable.unsubscribe();
    }
  }
}
