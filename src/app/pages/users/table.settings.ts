import { map, isUndefined } from 'lodash';

export const SETTINGS = {
  hideSubHeader: true,
  actions: {
    edit: false,
  },
  delete: {
    deleteButtonContent: '<i class="ion-trash-a"></i>',
    confirmDelete: true,
  },
  update: {
    updateButtonContent: '<i class="ion-trash-a"></i>',
    confirmUpdate: true,
  },
  columns: {
    _id: {
      title: 'ID',
      type: 'string',
      editable: false,
    },
    name: {
      title: 'Name',
      type: 'string',
    },
    role: {
      title: 'Role',
      type: 'string',
    },
    isVerified: {
      title: 'isVerified',
      type: 'boolean',
    },
    email: {
      title: 'Email',
      type: 'string',
    },
    createdAt: {
      title: 'Data Create',
      type: 'string',
    },
  },
};
