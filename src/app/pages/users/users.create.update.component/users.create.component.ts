/**
 * Created by smartit-11 on 26.07.17.
 */
import {Component, OnInit, OnDestroy, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {remove, forEach, find, keys} from 'lodash';
import './ckeditor.loader';
import 'ckeditor';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {Users} from '../Users';
import {UsersService} from '../users.service';

@Component({
  moduleId: module.id,
  selector: 'create-users-component',
  templateUrl: 'users.create.component.html',
  styleUrls: ['users.create.component.scss'],
})
export class CreateUsersComponent implements OnInit, OnDestroy {

  private createObservable: any;
  private getUsersObservable: any;
  private usersId: any;
  usersNew = new Users();
  isCreate = false;
  errors = '';

  config = {
    uiColor: '#F0F3F4',
    height: '400',
  };

  constructor(private service: UsersService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params['id']) {
        this.usersId = params['id'];
        this.getUsersObservable = this.service.getUsersById(params['id'])
          .map(users => users.json())
          .subscribe(res => {
            this.usersNew = new Users(res);
          });
        return params['id'];
      } else {
        this.isCreate = true;
      }
    });
  }


  onSubmit() {
    if (this.isCreate) {
      this.service.createUsers(this.usersNew)
        .map(res => res.json())
        .subscribe(res => {
          this.router.navigate([`/pages/users/users/${res._id}`]);
        }, err => {
          this.errors = err.json().message;
        });
    } else {
      this.service.updateUsers(this.usersNew, this.usersId)
        .map(res => res.json())
        .subscribe(res => {
          this.router.navigate([`/pages/users`]);
        }, err => {
          this.errors = err.json().message;
        });
    }
  }

  ngOnDestroy() {
    if (this.getUsersObservable) {
      this.getUsersObservable.unsubscribe();
    }
    if (this.createObservable) {
      this.createObservable.unsubscribe();
    }
  }
}
