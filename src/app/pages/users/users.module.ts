/**
 * Created by smartit-11 on 24.07.17.
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from "@angular/http";
import { DataTableModule } from "angular2-datatable";

import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CKEditorModule } from 'ng2-ckeditor';

import { NgaModule } from '../../theme/nga.module';
import { CreateUsersComponent } from './users.create.update.component/users.create.component';
import { SmartTablesModule } from '../general.components/smartTables/smartTables.module';
import { UsersComponentWrapper } from './users.component';
import { UsersService, UsersComponent, PrepaidUsersComponent, routing } from './index';
import { HttpClient } from '../httpClient';

@NgModule({
  imports: [
    SmartTablesModule,
    NgbDropdownModule,
    NgbModalModule,
    NgaModule,
    Ng2SmartTableModule,
    DataTableModule,
    HttpModule,
    CommonModule,
    FormsModule,
    routing,
    CKEditorModule,
  ],
  declarations: [
    PrepaidUsersComponent,
    UsersComponent,
    CreateUsersComponent,
    UsersComponentWrapper,
  ],
  entryComponents: [
    PrepaidUsersComponent,
    UsersComponent,
    CreateUsersComponent,
    UsersComponentWrapper,
  ],
  providers: [
    UsersService, HttpClient
  ],
})
export class UsersModule {
}
