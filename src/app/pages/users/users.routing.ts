import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.show.component/users.show.component';
import { PrepaidUsersComponent } from './prepaid.users.show.component/users.show.component';
import { UsersComponentWrapper } from './users.component';
import { CreateUsersComponent } from './users.create.update.component/users.create.component';

const routes: Routes = [
  {
    path: '',
    component: UsersComponentWrapper,
    children: [
      { path: '', component: UsersComponent },
      { path: 'create', component: CreateUsersComponent },
      { path: 'users/:id', component: CreateUsersComponent },
      { path: 'prepaid', component: PrepaidUsersComponent },
      { path: 'not-prepaid', component: PrepaidUsersComponent },
    ],
  },
];

export const routing = RouterModule.forChild(routes);


