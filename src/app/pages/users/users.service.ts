import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import { HttpClient } from '../httpClient';
import { Router } from "@angular/router";

import { environment } from '../../../environments/environment';
import { each } from "lodash";

// import {toPromise} from "rxjs/operator/toPromise";

@Injectable()
export class UsersService {
  constructor(private http: HttpClient,
              private router: Router) {
  }

  getProfile(): Observable<Response> {
    const url = `${environment.host}me`;
    return this.http.get(url)
      .catch(err => {

        switch (err.status) {
          case 401:
            localStorage.removeItem('token');
            this.router.navigate(['/']);
            break;
        }

        return err;
      });
  }

  getUsers(option = {}): Observable<Response> {
    let query;
    if (option) {
      query = '?';
      each(option, (value, key) => {
        query += `${key}=${value}`;
      })
    }

    const url = `${environment.host}users`;
    return this.http.get(`${url}${query}`)
      .catch(err => err);
  }

  // getUsers(): Observable<Response> {
  //   const url = `${environment.host}users`;
  //   return this.http.get(url)
  //     .catch(err => err);
  // }

  updateUsers(users, usersId): Observable<Response> {
    const url = `${environment.host}users/${usersId}`;
    return this.http.put(url, users);
  }

  deleteUsers(usersId): Observable<Response> {
    const url = `${environment.host}users/${usersId}`;
    return this.http.delete(url)
      .catch(err => err);
  }

  createUsers(users): Observable<Response> {
    const url = `${environment.host}users`;
    return this.http.post(url, users);
  }

  getUsersById(usersId): Observable<Response> {
    const url = `${environment.host}users/${usersId}`;
    return this.http.get(url)
      .catch(err => err);
  }
}
