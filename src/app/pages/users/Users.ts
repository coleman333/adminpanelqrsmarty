/**
 * Created by smartit-11 on 24.07.17.
 */
import { find, map } from 'lodash';

export class Users {
  _id: string;
  name: string;
  email: string;
  role: string;
  password: string;
  cteatedAt: string;

  constructor(public data?: object) {
    if (data) {
      this.setField(data);
    }
  }

  setField(data) {
    for ( let key in data) {
      this[key] = data[key];
    }
  }
}

