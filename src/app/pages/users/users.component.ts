import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'users-component-wrapper',
  templateUrl: 'users.component.html',
})
export class UsersComponentWrapper {}

