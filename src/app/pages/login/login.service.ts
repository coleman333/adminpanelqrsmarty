import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';
import { HttpClient } from '../httpClient';

@Injectable()
export class LoginService {
  constructor(private http: HttpClient) {}

  loginUser(product): Observable<Response> {
    const url = `${environment.host}login`;
    return this.http.post(url, product)
      .map(res => {
        if (res.json().response.role === 'admin'){
          localStorage.setItem('admin', 'true');
        }
        return res;
      })

  }
}
