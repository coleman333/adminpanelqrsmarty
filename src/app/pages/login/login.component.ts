import {Component} from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import 'rxjs/add/operator/catch';
import { LoginService } from './index';
import {Router, ActivatedRoute} from '@angular/router';
// import fbLogo from './images/Facebook.png';

@Component({
  selector: 'login',
  templateUrl: './login.html',
  styleUrls: ['./login.scss']
})
export class Login {

  public form:FormGroup;
  public email:AbstractControl;
  public password:AbstractControl;
  public submitted:boolean = false;
  errors = '';

  constructor(fb:FormBuilder, private service: LoginService,
              private router: Router, private route: ActivatedRoute) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });

    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];

    this.route.queryParams.subscribe(params => {
      if (params['token']) {
        localStorage.setItem('token', params['token']);
        this.router.navigate(['/dashboard']);
         this.router.navigate(['/products']);
      }
    })
  }

  public onSubmit(values: Object):void {
    this.submitted = true;
    if (this.form.valid) {
      this.service.loginUser(values)
        .subscribe(res => {
          localStorage.setItem('token', res.json().token);
          location.reload();
          this.router.navigate([`/pages/dashboard`]);
        }, err => {
          this.errors = err.json().message;
        });
    }
  }

}
