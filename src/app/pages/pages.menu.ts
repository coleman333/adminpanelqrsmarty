export const PAGES_MENU = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'general.menu.dashboard',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'products',
        data: {
          menu: {
            title: 'Products',
            icon: 'ion-android-cart',
            selected: false,
            expanded: false,
            order: 100,
          },
        },
      },
      {
        path: 'users',
        data: {
          menu: {
            title: 'Manage Users',
            icon: 'ion-android-contacts',
            selected: false,
            expanded: false,
            order: 0,
            hidden: localStorage.getItem('admin') !== 'true' ? true : false
          }
        },
      },
      {
        path: ['/', 'pages', 'users', 'prepaid'],
        data: {
          menu: {
            title: 'Prepaid Users',
            icon: 'ion-android-contacts',
            selected: false,
            expanded: false,
            order: 0,
            hidden: localStorage.getItem('admin') !== 'true' ? true : false
            //localStorage.setItem('admin','true')
          }
        },
      },
      {
        path: ['/', 'pages', 'users', 'not-prepaid'],
        data: {
          menu: {
            title: 'Not prepaid Users',
            icon: 'ion-android-contacts',
            selected: false,
            expanded: false,
            order: 0,
            hidden: localStorage.getItem('admin') !== 'true' ? true : false
          }
        },
      }
    ],
  },
  {
    path: 'logout',
    data: {
      menu: {
        title: 'Logout',
        icon: 'ion-android-exit',
        selected: false,
        expanded: false,
        order: 0
      }
    },
  },
];
