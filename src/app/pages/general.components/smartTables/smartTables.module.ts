/**
 * Created by smartit-11 on 28.06.17.
 */
import { NgModule } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { NgaModule } from '../../../theme/nga.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HttpModule } from "@angular/http";
import { DataTableModule } from "angular2-datatable";
import { SmartTables } from './smartTables.component';

@NgModule({
  imports: [
    NgbDropdownModule,
    NgbModalModule,
    NgaModule,
    Ng2SmartTableModule,
    DataTableModule,
    HttpModule,
    CommonModule,
    FormsModule,
  ],
  declarations: [
    SmartTables,
  ],
  exports: [
    SmartTables,
  ],
})
export class SmartTablesModule {}
