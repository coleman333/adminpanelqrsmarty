import { Component, Input } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'tables',
  templateUrl: 'smartTables.html',
  styleUrls: ['smartTables.scss'],
})
export class SmartTables {
  @Input()
  updateItem;

  @Input()
  deleteItem;

  @Input()
  selectItem;

  settings: object = {};
  source: LocalDataSource = new LocalDataSource();

  constructor(private router: Router) {}

  setSourse(data, settings) {
    this.source.load(data);
    this.settings = settings;
  }

  addData(data) {
    this.source.append(data);
  }

  productSelect(event): void {
    if (this.selectItem) {
      this.selectItem(event.data._id);
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.deleteItem(event.data._id);
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm() {}

  onSaveConfirm(event) {
    // if (window.confirm('Are you sure you want to save?')) {
    //   event.confirm.resolve(event.newData);
    //   this.updateItem(event.newData);
    // } else {
    //   event.confirm.reject();
    // }
  }
}
