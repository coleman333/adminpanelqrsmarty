import {Component} from '@angular/core';
import 'rxjs/add/operator/catch';
import {Router} from '@angular/router';


@Component({
  selector: 'logout',
  templateUrl: './logout.html',
  styleUrls: ['./logout.scss']
})
export class Logout {

  constructor(private router: Router) {
    localStorage.removeItem('token');
    localStorage.removeItem('admin');
    this.router.navigate([`/login`]);
  }

}
