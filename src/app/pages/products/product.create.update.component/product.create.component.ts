/**
 * Created by smartit-11 on 26.07.17.
 */
import {Component, OnInit, OnDestroy, ElementRef, ViewChild, ViewChildren, QueryList, AfterViewInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {remove, forEach, find, keys} from 'lodash';

import './ckeditor.loader';

import {Product} from '../Product';
import {ProductsService} from '../product.service';
import {BaThemeSpinner} from '../../../theme/services';
import {CKEditorComponent} from "ng2-ckeditor";

@Component({
  moduleId: module.id,
  selector: 'create-product-component',
  templateUrl: 'product.create.component.html',
  styleUrls: ['product.create.component.scss'],
})
export class CreateProductComponent implements OnInit, OnDestroy {
  @ViewChild("qrcode") qrcode: ElementRef;
  @ViewChild("ckeditorEquipment") ckeditorEquipment: any;

  private subcategoriesObservable: any;
  private createObservable: any;
  private getProductObservable: any;
  private productId: any;
  responceQRCode: string;
  subCategories: any;
  loadImage = {};
  loadDocument = {};
  documents = [];
  productNew = new Product();
  isCreate = false;
  subcategory: any;

  config = {
    uiColor: '#F0F3F4',
    height: '400',
  };

  constructor(private service: ProductsService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private _spinner: BaThemeSpinner) {

  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params['id']) {
        this.productId = params['id'];
        this.getProductObservable = this.service.getProductById(params['id'])
          .map(product => product.json())
          .subscribe(res => {
            this.productNew = new Product(res);
            if (res.documents) {
              this.documents = res.documents;
            }

            console.log(this.ckeditorEquipment);
            this.ckeditorEquipment;

            this.responceQRCode = res.qrCode;
            this.qrcode.nativeElement.insertAdjacentHTML('beforeend', this.responceQRCode);
            this.getOtherSubcategoriesForProduct();
            console.log(this.productNew);
          });
        return params['id'];
      } else {
        this.isCreate = true;
        this.getSubcategories();
      }
    });

  }

  getOtherSubcategoriesForProduct() {
    const arrayIdSubcategory = [];
    find(this.productNew.getSubcategoryId, subcategory => {
      arrayIdSubcategory.push(subcategory['_id']);
    });

    this.subcategoriesObservable = this.service.getOtherSubcategoriesForProduct(arrayIdSubcategory)
      .map(subcategory => subcategory.json())
      .subscribe(res => {
        this.subCategories = res;
      });
  }

  getSubcategories() {
    this.subcategoriesObservable = this.service.getSubcategories()
      .map(subcategories => subcategories.json())
      .subscribe(res => {
        this.subCategories = res;
      });
  }

  removeImageDescription(img, type) {
    remove(this.productNew[type].images, image => image === img);
  }

  removeImageProduct(img) {
    remove(this.productNew.images, image => image === img);
  }

  onChange(event, title) {
    title = title ? title : 'product';
    let isExist = false;

    if (this.loadImage[title]) {
      isExist = true;
    }

    if (!isExist) {
      this.loadImage[title] = [this.handlerGenerateObjectImage(event.srcElement.files[0])];
    } else {
      this.loadImage[title].push(this.handlerGenerateObjectImage(event.srcElement.files[0]));
    }
  }

  onChangeDocument(event, title) {
    title = title ? title : 'files';
    let isExist = false;

    if (this.loadDocument[title]) {
      isExist = true;
    }

    if (!isExist) {
      this.loadDocument[title] = [this.handlerGenerateObjectDocument(event.srcElement.files[0])];
    } else {
      this.loadDocument[title].push(this.handlerGenerateObjectDocument(event.srcElement.files[0]));
    }
  }

  handlerGenerateObjectImage(file) {
    return {
      imageName: file.name,
      file,
    };
  }

  handlerGenerateObjectDocument(file) {
    return {
      documentName: file.name,
      file,
    };
  }

  async onSubmit() {
    let formData = new FormData();
    const imageCategory = keys(this.loadImage);
    if (imageCategory) {
      forEach(imageCategory, (category) => {
        forEach(this.loadImage[category], (image, index) => {
          formData.append(`${category}${index}`, image.file);
        });
      });
    }
    const documentCategory = keys(this.loadDocument);
    if (documentCategory) {
      forEach(documentCategory, (category) => {
        forEach(this.loadDocument[category], (document) => {
          formData.append(`${category}`, document.file);
        });
      });
    } else {
      let emptyFiles = 'empty';
      formData.append('files.files', emptyFiles);
    }
    if (this.subcategory){
      this.productNew.nameSubCategory = this.subcategory;
      this.productNew.productName = this.productNew.name;
      // formData.append('data', JSON.stringify(this.productNew));
      // await this.service.createSubcategory({ name: this.subcategory, productName: this.productNew.name }).subscribe();
      // console.log(123434245, this.subcategory, this.productNew.name);
    }
    formData.append('data', JSON.stringify(this.productNew));
    // const result = JSON.stringify(formData);
    // console.log('this is all product formdata', result);
    // console.log('this is name of the new product', this.productNew.name);
    this._spinner.show();

    if (this.isCreate) {
      this.service.createProduct(formData)
        .map(res => res.json())
        .subscribe(res => {
          this._spinner.hide();
          this.router.navigate([`/pages/products/product/${res._id}`]);
        });
    } else {
      this.service.updateProduct(formData, this.productId)
        .map(res => res.json())
        .subscribe(res => {
          this._spinner.hide();
          this.router.navigate([`/pages/products`]);
        });

    }
  }

  addSubcategoryProduct(subcategory) {
    this.productNew.subcategoryId.push(subcategory);
    remove(this.subCategories, sub => sub['_id'] === subcategory._id);
  }

  removeSubcategoryProduct(subCategory) {
    const removeSub = remove(this.productNew.getSubcategoryId, sub => sub['_id'] === subCategory)[0];
    this.subCategories.push(removeSub);
  }

  removeImage(lastModified, title): void {
    remove(this.loadImage[title], image => image['file'].lastModified === lastModified);
  }

  removeDocument(document, title): void {

    const index = this.loadDocument[title].findIndex(x => x.documentName === document.documentName);
    this.loadDocument[title].splice(index, 1);

  }

  deleteDocument(document) {
    this.service.deleteDocument(document._id)
      .map(res => res.json())
      .subscribe(res => {
        const index = this.documents.findIndex(x => x.documentName === document.documentName);
        this.documents.splice(index, 1);
      });

  }

  getImg(product, format) {
    let svg = document.querySelector("svg");
    let svgData = new XMLSerializer().serializeToString(svg);

    let canvas = document.createElement("canvas");
    let svgSize = svg.getBoundingClientRect();
    canvas.width = svgSize.width;
    canvas.height = svgSize.height;
    let ctx = canvas.getContext("2d");
    console.log();
    if (format === 'jpeg') {
      ctx.fillStyle = '#fff';
      ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    let img = document.createElement("img");
    img.setAttribute("src", "data:image/svg+xml;base64," + btoa(svgData));

    img.onload = function () {
      ctx.drawImage(img, 0, 0);

      let imgsrc = canvas.toDataURL("image/" + format);

      let a = document.createElement("a");
      a.download = product.name + "QR." + format;
      a.href = imgsrc;
      a.click();
      a.remove();
    };
  }

  ngOnDestroy() {
    // this.getProductObservable.unsubscribe();
    if (this.subcategoriesObservable && this.createObservable) {
      this.subcategoriesObservable.unsubscribe();
      this.createObservable.unsubscribe();
    }
  }
}
