import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { SmartTables } from '../../general.components/smartTables/smartTables.component';
import { SETTINGS } from '../table.settings';
import { ProductsService } from '../product.service';
import { Product } from '../Product';

@Component({
  moduleId: module.id,
  selector: 'products-component',
  templateUrl: 'products.show.component.html',
  styleUrls: ['products.show.component.scss'],
})
export class ProductComponent implements OnInit, OnDestroy {

  @ViewChild(SmartTables)
  tables: SmartTables;

  private productsObservable: any;
  private editObservable: any;
  private deleteObservable: any;

  constructor(private service: ProductsService, private router: Router) {
    this.editItem = this.editItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  ngOnInit() {
    this.productsObservable = this.service.getProducts()
      .map(products => products.json().map(product => new Product(product)))
      .subscribe(products => {
        this.tables.setSourse(products, SETTINGS);
      });
  }

  editItem(data) {
    this.editObservable = this.service.updateProduct(data, data.id)
      .subscribe( res => {});
  }

  deleteItem(productId) {
    this.deleteObservable = this.service.deleteProduct(productId)
      .subscribe( res => {});
  }

  selectItem(productId) {
    this.router.navigate(['/pages/products/product/', productId ]);
  }

  ngOnDestroy() {
    if (this.productsObservable) {
      this.productsObservable.unsubscribe();
    }
    if (this.editObservable) {
      this.editObservable.unsubscribe();
    }
    if (this.deleteObservable) {
      this.deleteObservable.unsubscribe();
    }
  }
}
