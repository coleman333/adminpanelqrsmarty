/**
 * Created by smartit-11 on 24.07.17.
 */
import { find, map } from 'lodash';

export class Product {
  name: string;
  price: number;
  subcategoryId: object[] = [];
  images: string[] = [];
  serialNumber: string;
  availability: boolean;
  deliveryType: string;
  dataCreate: string;
  applications = {
    description: '',
    images: [],
  };
  equipment = {
    description: '',
    images: [],
  };
  exploitation = {
    description: '',
    images: [],
  };
  possibleMalfunctions = {
    description: '',
    images: [],
  };
  adjacentNodes = {
    description: '',
    images: [],
  };
  securityMeasures = {
    description: '',
    images: [],
  };
  callingCenter = {
    description: '',
    images: [],
  };
  productName: any;
  nameSubCategory: any;


  constructor(public data?: object) {
    if (data) {
      this.setField(data);
    }
  }

  setField(data) {
    for ( let key in data) {
      this[key] = data[key];
    }
  }

  get getSubcategoryId(): Object[] {
    return this.subcategoryId;
  }
}

