import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './products.show.component/products.show.component';
import { ProductComponentWrapper } from './product.component';
import { CreateProductComponent } from './product.create.update.component/product.create.component';

const routes: Routes = [
  {
    path: '',
    component: ProductComponentWrapper,
    children: [
      { path: '', component: ProductComponent },
      { path: 'create', component: CreateProductComponent },
      { path: 'product/:id', component: CreateProductComponent },
    ],
  },
];

export const routing = RouterModule.forChild(routes);

