import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'products-component-wrapper',
  templateUrl: 'product.component.html',
})
export class ProductComponentWrapper {}
