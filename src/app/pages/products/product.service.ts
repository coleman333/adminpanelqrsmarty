import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import { HttpClient } from '../httpClient';

import { environment } from '../../../environments/environment';
import {toPromise} from "rxjs/operator/toPromise";

@Injectable()
export class ProductsService {
  constructor(private http: HttpClient) {}

  createSubcategory(subcategory): Observable<Response> {
    console.log(subcategory);
    const url = `${environment.host}subcategories`;
    return this.http.post(url, subcategory)
      .catch(err => err);
  }

  getProducts(): Observable<Response> {
    const url = `${environment.host}products`;
    return this.http.get(url)
      .catch(err => err);
  }

  getSubcategories(): Observable<Response> {
    const url = `${environment.host}subcategories`;
    return this.http.get(url)
      .catch(err => err);
  }

  getOtherSubcategoriesForProduct(arrayId): Observable<Response> {
    const url = `${environment.host}subcategories/other`;
    return this.http.post(url, arrayId)
      .catch(err => err);
  }

  updateProduct(product, productId): Observable<Response> {
    const url = `${environment.host}product/${productId}`;
    return this.http.put(url, product)
      .catch(err => err);
  }

  deleteProduct(productId): Observable<Response> {
    const url = `${environment.host}product/${productId}`;
    return this.http.delete(url)
      .catch(err => err);
  }

  deleteDocument(documentId): Observable<Response> {
    const url = `${environment.host}document/${documentId}`;
    return this.http.delete(url)
      .catch(err => err);
  }

  createProduct(product): Observable<Response> {
    const url = `${environment.host}products`;
    return this.http.post(url, product)
      .catch(err => err);
  }

  getProductById(productId): Observable<Response> {
    const url = `${environment.host}product/${productId}`;
    return this.http.get(url)
      .catch(err => err);
  }
}
