import { map, isUndefined } from 'lodash';

export const SETTINGS = {
  hideSubHeader: true,
  actions: {
    edit: false,
  },
  delete: {
    deleteButtonContent: '<i class="ion-trash-a"></i>',
    confirmDelete: true,
  },
  columns: {
    id: {
      title: 'ID',
      type: 'string',
      editable: false,
    },
    name: {
      title: 'Name',
      type: 'string',
    },
    price: {
      title: 'Price',
      type: 'number',
    },
    subcategoryId : {
      title: 'Subcategory',
      valuePrepareFunction: (values) => map(values, val => val['name']),
      type: 'string',
    },
    images: {
      title: 'Images',
      type: 'string',
    },
    serialNumber: {
      title: 'Serial Number',
      type: 'string',
    },
    availability: {
      title: 'Availability',
      filter: false,
      editor: {
        type: 'list',
        config: {
          list: [
            { value: true, title: 'true' },
            { value: false, title: 'false' },
          ],
        },
      },
    },
    deliveryType: {
      title: 'Delivery type',
      filter: false,
      editor: {
        type: 'list',
        config: {
          list: [
            { value: 'Plane', title: 'Plane' },
            { value: 'Car', title: 'Car' },
            { value: 'Train', title: 'Train' },
            { value: 'Ship', title: 'Ship' },
          ],
        },
      },
    },
    dataCreate: {
      title: 'Data Create',
      type: 'string',
    },
  },
};
