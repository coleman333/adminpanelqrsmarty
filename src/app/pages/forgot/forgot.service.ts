import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';
import { HttpClient } from '../httpClient';

@Injectable()
export class ForgotService {
  constructor(private http: HttpClient) {}

  resetUser(product): Observable<Response> {
    const url = `${environment.host}reset`;
    return this.http.post(url, product);
  }
}
