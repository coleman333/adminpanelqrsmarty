import {Component} from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import 'rxjs/add/operator/catch';
import { ForgotService } from './index';
import {Router} from '@angular/router';


@Component({
  selector: 'forgot',
  templateUrl: './forgot.html',
  styleUrls: ['./forgot.scss']
})
export class Forgot {

  public form:FormGroup;
  public email:AbstractControl;
  public submitted:boolean = false;
  errors = '';
  verification = '';

  constructor(fb:FormBuilder, private service: ForgotService,
              private router: Router) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });

    this.email = this.form.controls['email'];
  }

  public onSubmit(values: Object): void {
    this.submitted = true;
    if (this.form.valid) {
      this.service.resetUser(values)
        .subscribe(res => {
          this.verification = res.json().message;
          // this.router.navigate([`/login`]);
        }, err => {
          this.errors = err.json().message;
        });
    }
  }
}
