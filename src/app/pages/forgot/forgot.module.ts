import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { AppTranslationModule } from '../../app.translation.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { Forgot } from './forgot.component';
import { routing }  from './forgot.routing';
import { ForgotService } from './index';
import { HttpClient } from '../httpClient';


@NgModule({
  imports: [
    CommonModule,
    AppTranslationModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing
  ],
  declarations: [
    Forgot
  ],
  providers: [
    ForgotService,
    HttpClient
  ]
})
export class ForgotModule {}
