import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import { environment } from '../../../environments/environment';
import { HttpClient } from '../httpClient';


@Injectable()
export class RegisterService {
  constructor(private http: HttpClient) {}

  registerUser(product): Observable<Response> {
    const url = `${environment.host}register`;
    return this.http.post(url, product);
  }
}
