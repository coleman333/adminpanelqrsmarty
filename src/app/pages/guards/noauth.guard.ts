import {Injectable} from '@angular/core';
import {Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';

@Injectable()
export class NoAuthGuard implements CanActivate {
  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if (localStorage.getItem('token')) {
      this.router.navigate(['/pages/dashboard']);
      this.router.navigate(['/pages/products']);
      this.router.navigate(['/pages/users']);
      return false;
    } else {
      return true;
    }

  }
}

