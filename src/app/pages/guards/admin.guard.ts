import {Injectable} from '@angular/core';
import {Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if (localStorage.getItem('admin')) {
      return true;
    } else {
      this.router.navigate(['/pages/dashboard']);
      this.router.navigate(['/pages/products']);
      return false;
    }

  }
}

