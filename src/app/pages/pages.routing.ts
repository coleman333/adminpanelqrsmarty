import { Routes, RouterModule } from '@angular/router';
import { Pages } from './pages.component';
import { ModuleWithProviders } from '@angular/core';
import { AuthGuard } from './guards/auth.guard'
import { AdminGuard } from './guards/admin.guard'
import { NoAuthGuard } from './guards/noauth.guard';
import { UserDataResolver } from './guards/UserDataResolver';


// export function loadChildren(path) { return System.import(path); };

export const routes: Routes = [
  {
    path: 'login',
    canActivate: [NoAuthGuard],
    loadChildren: 'app/pages/login/login.module#LoginModule'
  },
  {
    path: 'register',
    canActivate: [NoAuthGuard],
    loadChildren: 'app/pages/register/register.module#RegisterModule'
  },
  {
    path: 'logout',
    canActivate: [AuthGuard],
    loadChildren: 'app/pages/logout/logout.module#LogoutModule'
  },
  {
    path: 'forgot',
    canActivate: [NoAuthGuard],
    loadChildren: 'app/pages/forgot/forgot.module#ForgotModule'
  },
  {
    path: 'pages',
    component: Pages,
    canActivate: [AuthGuard],
    resolve: {
      userData: UserDataResolver
    },
    children: [
      { path: '', redirectTo: 'products', pathMatch: 'full' },
      { path: 'products', loadChildren: './products/product.module#ProductModule' },
      { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
      {
        path: 'users',
        canActivate: [AdminGuard],
        children: [
          {
            path: '',
            loadChildren: './users/users.module#UsersModule'
          },
          {
            path: 'prepaid',
            loadChildren: './users/users.module#UsersModule'
          },
          {
            path: 'not-prepaid',
            loadChildren: './users/users.module#UsersModule'
          },
        ]
      },
    ]
  }

];


export const routing: ModuleWithProviders = RouterModule.forChild(routes);
